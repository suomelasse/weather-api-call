import requests
import json
from datetime import datetime

# app id of https://home.openweathermap.org/api_keys
app_id = ''

# coordinates of Oulu:
latitude = 65.012335
longitude = 25.469475

# we want daily weather not current, hourly or minutely so we exclude those in the API call
r = requests.get(f'https://api.openweathermap.org/data/2.5/onecall?exclude=minutely,hourly,current&cnt=3&lat={latitude}&lon={longitude}&units=metric&appid={app_id}')

# get the content of the request
content = r.content

try:
    data = json.loads(content)
except Exception as e:
    print(e)

tz = data['timezone']  # get the timezone of the API call

# how many days shall we show
day_range = 2

print(f'Forecast for {day_range} days:')
print(f'Timezone: {tz}\n')

# for loop from x to day_range
for i in range(day_range):
    temp = data['daily'][i]['temp']['day']  # temperature
    uv = data['daily'][i]['uvi']  # uv index
    weather = data['daily'][i]['weather'][0]['main']
    humi = data['daily'][i]['humidity']  # humidity
    pop = round(data['daily'][i]['pop'] * 100)  # probability of precipitation * 100 to get %
    pres = data['daily'][i]['pressure']  # pressure
    dt = data['daily'][i]['dt']  # timestamp of the weather report

    day = datetime.fromtimestamp(dt)

    # detect if there is a chance to rain
    if 'rain' in data['daily'][i]:
        rain = data['daily'][i]['rain']
    else:
        rain = 0

    print(day.strftime('%A:'))  # get the day of the week from the timestamp

    print(f'Temperature: {temp} °C')
    print(f'Humidity: {humi} %')
    print(f'Pressure: {pres} hPa')
    print(f'Weather: {weather}')
    print(f'Chance of rain: {pop} %')
    print(f'Precipitation: {rain} mm')
    print(f'UV: {uv}\n')
