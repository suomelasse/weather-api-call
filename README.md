## Get weather data for specific coordinates using OWM API
This project will use Python to create GET request to Open Weather Map API and it will request forecast data for x amount of days. You need an [OWM api key](https://home.openweathermap.org/api_keys) to make these requests.

---
## Usage
All you need is the api key and coordinates of the place you want to check weather data for.

These are the variables you should change:
```
# app id of https://home.openweathermap.org/api_keys
app_id = ''

# coordinates of Oulu:
latitude = 65.012335
longitude = 25.469475

# how many days shall we show
day_range = 2
```
---
## Data will look like this:
```
Forecast for 2 days:
Timezone: Europe/Helsinki

Thursday:
Temperature: -4.74 °C
Humidity: 93 %
Pressure: 993 hPa
Weather: Clouds
Chance of rain: 0 %
Precipitation: 0 mm
UV: 0.16

Friday:
Temperature: -11.78 °C
Humidity: 95 %
Pressure: 1004 hPa
Weather: Snow
Chance of rain: 36 %
Precipitation: 0 mm
UV: 0.17
```